import { ServiceService } from './../services/service.service';
import { Service } from './service';
import { LanguageService } from '../services/language.service';

export class ServiceOption {
    id?: string;
    projectId?: string;
    displayId?: string;
    relativeIdList?: [];

    code?: string;

    serviceId?: string;

    optionParmas?: {};
    imgKeyList?: [];

    categoryIdList?: [];
    attributeIdList?: [];

    retailPrice?: number;
    sellingPrice?: number;
    discount?: number;
    currencyId?: string;

    stock?: number;

    priority?: number;
    status?: string;

    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    name?: string;
    service?: Service;

    constructor(
        private languageService?: LanguageService,
        private serviceService?: ServiceService,
    ) {

    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.displayId = params.displayId || '';
        this.relativeIdList = params.relativeIdList || [];

        this.code = params.code || '';

        this.serviceId = params.serviceId || '';
        this.imgKeyList = params.imgKeyList || [];
        this.optionParmas = params.optionParmas || {};

        this.retailPrice = params.retailPrice || undefined;
        this.sellingPrice = params.sellingPrice || undefined;
        this.discount = params.discount || undefined;
        this.currencyId = params.discocurrencyIdunt || '';

        this.stock = params.stock || undefined;

        this.priority = params.priority || undefined;
        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
    }

    async loadName(params): Promise<string> {
        return this.languageService ? await this.languageService.getParam('shop', params.id, 'name') : '';
    }

    async loadService(params): Promise<Service> {
        let service = new Service();

        if (this.languageService) {
            service = new Service(this.languageService);
        }

        if (this.serviceService) {

            const getServiceParams = {
                id: params.serviceId,
                status: 'enable',
            };
            const response = await this.serviceService.get(getServiceParams);
            const responseItem = response.pop();

            service.init(responseItem);
        }
        this.service = service;
        return service;
    }
}
