import { User } from './user';

import { Service } from './service';
import { Shop } from './shop';

import { Time } from './time';

export class Booking {
    id?: string;
    projectId?: string;

    userId: string;
    merchantId: string;
    shopId: string;

    user?: User;
    service?: Service;
    shop?: Shop;

    date?: Date;
    time?: Time;

    numberOfPeople?: number;
    guestList?: [];

    status?: 'opened' | 'initiated' | 'confirmed' | 'completed' | 'cancelled' | 'onhold';
    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    constructor() {
    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.user = new User();
        if (params.user) {
            this.user.init(params.user);
        }

        this.service = new Service();
        if (params.service) {
            this.service.init(params.service);
        }

        this.shop = new Shop();
        if (params.shop) {
            this.shop.init(params.shop);
        }

        if (params.date) {
            switch (typeof params.date) {
                case 'string':
                    this.date = new Date(params.date);
                case 'object':
                    this.date = params.date;
                default:
                    this.date = new Date();
            }
        }

        this.time = new Time();
        if (params.time) {
            this.time.init(params.time);
        }

        this.numberOfPeople = params.numberOfPeople || 0;

        this.guestList = params.guestList || [];

        this.status = params.status || 'opened';
        this.tagList = params.tagList || [];

        this.updatedAt = params.updatedAt;
        this.createdAt = params.createdAt;
    }
}
