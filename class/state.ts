
var generator = require('secure-random-password');

export class Auth {
    id            : string;
    code          : string;
    countryId     : string;

	constructor() {
	}

    init(params){
        this.id             = params.id             || "";
        this.code          = params.email          || "";
        this.countryId     = params.countryId       || "";
    }
}