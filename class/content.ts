import { environment } from '../../../environments/environment';

import { LanguageService } from '../services/language.service';

import { Shop } from './shop';
import { Service } from './service';
import { Product } from './product';


export class ContentDocument {
    id?: string;
    type?: string;
    code?: string;

    class?: string;

    title?: string;
    searchParams?: {};
    searchTerm?: string;
    searchQuery?: {};

    htmlString?: string;
    style?: { [klass: string]: any; };

    classList?: [];
    classListString?: string;

    options?: any;

    tabList?: {};
    activeTab?: string;

    genre?: string;

    from?: number;
    limit?: number;

    shopList?: Shop[];
    serviceList?: Service[]
    productList?: Product[]

    constructor(params = {}) {
        if (params) {
            this.init(params);
        }
    }

    async init(params) {
        this.id = params.id || '';
        this.type = params.type || '';
        this.code = params.code || '';

        this.class = params.class || '';

        this.title = params.title || '';
        this.searchParams = params.searchParams || undefined;
        this.searchTerm = params.searchTerm || '';
        this.searchQuery = params.searchQuery || undefined;

        this.htmlString = params.htmlString || '';
        this.style = params.style || {};

        this.classList = params.classList || [];
        this.classListString = params.classList || this.classList.join(' ') || '';

        this.options = params.options || {};

        this.tabList = params.tabList || {};
        this.activeTab = params.activeTab || {};

        this.genre = params.genre || '';
    }
}

export class Content {
    id?: string;
    projectId?: string;


    application?: string;
    type?: string;
    code?: string;

    document?: ContentDocument[];

    htmlString?: string;
    style?: { [klass: string]: any; };

    classList?: [];
    classListString?: string;

    status?: string;
    tagList: [];

    updatedAt?: number;
    createdAt?: number;

    title?: string;
    name?: string;

    imgKey?: string

    constructor(
        private languageService?: LanguageService,
    ) { }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.application = params.application || '';
        this.type = params.type || '';
        this.code = params.code || '';

        this.document = [];
        await this.loadDocumentList(params.document || []);

        this.htmlString = params.htmlString || '';
        this.style = params.style || {};

        this.classList = params.classList || [];
        this.classListString = params.classList || this.classList.join(' ') || '';

        this.status = params.status || 'disable';
        this.tagList = params.tagList || [];

        this.title = params.title || '';
        this.name = await this.loadName(params);

        this.imgKey = params.imgKey || '';
    }

    getImgUrl(column) {
        return environment.media.url + '/' + this[column];
    }

    async loadDocumentList(documentList) {
        for (const documnet of documentList) {
            const contentDocument = new ContentDocument(documnet);
            this.document.push(contentDocument);
        }
        return this.document;
    }

    async setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    async loadName(params): Promise<string> {
        this.name = this.languageService ? await this.languageService.getParam('category', params.id, 'name') : '';
        return this.name;
    }
}
