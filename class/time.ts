export class Time {
    status?: string = 'disable';
    hour?: number = 10;
    minute?: number = 0;

    constructor(params?) {
        this.init(params);
    }

    async init(params) {
        if (params) {
            if (typeof params.hour !== 'undefined') {
                this.hour = params.hour;
            }
            if (typeof params.minute !== 'undefined') {
                this.minute = params.minute;
            }
        } else {
            this.hour = 10;
            this.minute = 0;
        }
    }

    getDisplayDate() {
        const dateString = 'Jan 01, 2001 ' + this.hour + ':' + this.minute;
        return new Date(dateString);
    }

    getDisplayTime(): string {
        let timeString = '';

        if (this.hour < 10) {
            timeString += '0';
        }
        timeString += this.hour + ':';

        if (this.minute < 10) {
            timeString += '0';
        }
        timeString += this.minute;

        return timeString;
    }

    setTimeFromDate(date: Date) {
        this.hour = date.getHours();
        this.minute = date.getMinutes();
    }

    setTimeFromString(string) {
        const stringArray = string.split(':');
        this.minute = parseInt(stringArray.pop()) || 0;
        this.hour = parseInt(stringArray.pop()) || 10;
    }
}