import { environment } from './../../../environments/environment';
import { LanguageService } from './../services/language.service';


export class Payment {
    id?: string;
    projectId?: string;

    code?: string;
    iconKey?: string;
    status: string;
    tagList: [];

    name?: string;

    constructor(private languageService?: LanguageService,
    ) {}


    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.code = params.code || '';
        this.iconKey = params.iconKey || '';
        this.status = params.status || 'disable';
        this.tagList = params.tagList || [];

        await this.loadName(params);
    }

    setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    getIconUrl() {
        if (!this.iconKey) {
            return undefined;
        }
        return environment.media.url + '/' + this.iconKey;
    }

    async loadName(params) {
        this.name =  this.languageService ? await this.languageService.getParam('payment', params.id, 'name') : '';
    }
}
