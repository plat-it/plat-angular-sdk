import { ContentDocument } from "./content";

export class Banner {
    id?: string;
    projectId?: string;

    code?: string;

    imgKey?: string;

    groupId: string;

    conditionParams?: {};
    bannerStyle?: {};

    endpoint?: string;
    endpointValue?: {};

    document?: ContentDocument[];

    version?: string;

    priority?: number;

    status?: string;
    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    constructor() { }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.code = params.code || '';

        this.groupId = params.groupId || '';

        this.conditionParams = params.conditionParams || {};
        this.bannerStyle = params.bannerStyle || {};

        this.endpoint = params.endpoint || '';
        this.endpointValue = params.endpointValue || {};

        this.imgKey = params.imgKey || '';

        this.priority = params.priority || 10;

        this.version = params.version || '';
        this.status = params.status || 'disable';
        this.tagList = params.tagList || [];

        this.document = [];
        await this.loadDocumentList(params.document);
    }


    async initFromDocument(params) {
        this.code = params.code || '';

        this.conditionParams = {}
        this.loadConditionParams(params);


        this.bannerStyle = params.bannerStyle || {};

        this.endpoint = params.endpoint || '';
        this.endpointValue = params.endpointValue || {};

        this.imgKey = params.imageLink || '';

        this.priority = params.priority || undefined;

        this.status = params.status || 'disable';
    }

    async loadDocumentList(documentList: [] = []) {
        for (const documnet of documentList) {
            const contentDocument = new ContentDocument(documnet);
            this.document.push(contentDocument);
        }
        return this.document;
    }

    async loadConditionParams(params) {
        const sectorIdList = [];
        const categoryIdList = [];

        if (params.criteria) {
            if (params.criteria.sectorId) {
                sectorIdList.push(params.criteria.sectorId)
            }
            if (params.criteria.categoryId) {
                categoryIdList.push(params.criteria.categoryId)
            }
        }
        console.log(params.criteria);

        this.conditionParams = {
            filter: {
                sectorIdList: sectorIdList,
                categoryIdList: categoryIdList,
            }
        }
    }
}
