import { environment } from '../../../environments/environment';


export class Message {
    id: string;
    projectId?: string;

    roomId?: string;
    userId?: string;

    content?: string;
    imgKeyList?: string;

    status?: string;

    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    constructor() { }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.roomId = params.roomId || '';
        this.content = params.content || '';
        this.imgKeyList = params.imgKeyList || [];

        this.status = params.status || 'opened';
        this.tagList = params.tagList || [];

    }

    getImgUrl(column) {
        return environment.media.url + '/' + this[column];
    }

}
