export class Permission {
    id: string;
    authId: string;
    projectId: string;

    permissionRole: string;

    status?: string;

    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    constructor() {
    }

    init(params) {
        this.id = params.id || '';
        this.authId = params.authId || '';

        this.permissionRole = params.permissionRole || 'guest';

        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];

        this.updatedAt = params.updatedAt || undefined;
        this.createdAt = params.createdAt || undefined;
    }
}
