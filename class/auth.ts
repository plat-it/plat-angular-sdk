
export class Auth {
    id: string;
    projectId: string;
    email: string;
    hasPassword: string;
    password: string;
    tagList: [];
    updatedAt: number;
    createdAt: number;

    constructor() {
    }

    init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.email = params.email || '';

        this.password = params.password || '';
        this.hasPassword = params.hasPassword || '';

        this.tagList = params.tagList || [];

        this.updatedAt = params.updatedAt || undefined;
        this.createdAt = params.createdAt || undefined;
    }
}
