import { environment } from '../../../environments/environment';
export class User {
    id: string;
    displayId: string;

    authId: string;

    merchantIdList: string[];
    shopIdList: string[];

    categoryIdList: string;
    stripeCustomerIdList: [];

    avatarImgKey: string;
    imgKeyList: string[];

    first: string;
    middle: string;
    last: string;

    email: string;
    emailVerified: boolean;
    phone: number;
    areaCode: string;
    phoneVerified: boolean;
    whatsapp: number;
    whatsappAreaCode: string;
    whatsappVerified: boolean;

    vaccinationStatus: string;
    qualification: string;

    age: number;
    gender: string;

    settings: {};
    defaultLanguage: string;
    languageList: string[];

    status: string;

    tagList: [];

    updatedAt: number;
    createdAt: number;

    constructor() {
    }

    init(params) {
        this.id = params.id || '';
        this.displayId = params.displayId || '';

        this.authId = params.authId || '';

        this.merchantIdList = params.merchantIdList || [];
        this.shopIdList = params.shopIdList || [];

        this.categoryIdList = params.categoryIdList || [];
        this.stripeCustomerIdList = params.stripeCustomerIdList || [];

        this.avatarImgKey = params.avatarImgKey || '';
        this.imgKeyList = params.imgKeyList || '';

        this.first = params.first || '';
        this.middle = params.middle || '';
        this.last = params.last || '';

        this.email = params.email || '';
        this.emailVerified = params.emailVerified || false;
        this.phone = params.phone || '';
        this.areaCode = params.areaCode || '+852';
        this.phoneVerified = params.phoneVerified || false;
        this.whatsapp = params.whatsapp || '';
        this.whatsappAreaCode = params.whatsappAreaCode || '+852';
        this.whatsappVerified = params.whatsappVerified || false;

        this.vaccinationStatus = params.vaccinationStatus || '';
        this.qualification = params.qualification || '';

        this.age = params.age || undefined;
        this.gender = params.gender || '';

        this.settings = params.settings || {};
        this.defaultLanguage = params.defaultLanguage || '';
        this.languageList = params.languageList || [];

        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];
        this.updatedAt = params.aupdatedAtge || undefined;
        this.createdAt = params.createdAt || undefined;
    }

    async initFromShopDocument(params) {
        this.email = params['Email'] || '';
        this.phone = params['WhatsApp'] || undefined;
        this.whatsapp = params['WhatsApp'] || undefined;
        this.status = params.status || 'disable';
    }

    getName() {
        const name = [];
        if (this.first) {
            name.push(this.first);
        }
        if (this.middle) {
            name.push(this.middle);
        }
        if (this.last) {
            name.push(this.last);
        }
        return name.join(' ');
    }
}
