import { environment } from '../../../environments/environment';
import { Message } from './message';

export class Room {
    id?: string;
    projectId?: string;

    imgKeyList?: string[];

    userIdList?: number;
    name?: string;
    status?: string;

    tagList: [];

    updatedAt?: number;
    createdAt?: number;

    isTyping?: boolean;
    isActive?: boolean;

    lastMessage?: Message;

    constructor() { }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.name = params.name || '';

        this.imgKeyList = params.imgKeyList || [];
        this.userIdList = params.userIdList || [];

        this.status = params.status || 'disabled';

        this.tagList = params.tagList || [];
    }

}
