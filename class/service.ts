import { LanguageService } from '../services/language.service';
import { ShopService } from '../services/shop.service';

import { Payment } from './payment';
import { Shop } from './shop';

export class Service {
    id?: string;
    projectId?: string;
    displayId?: string;
    relativeIdList?: [];

    code?: string;

    genre?: string;

    shopId?: string;
    shopCode?: string;
    merchantId?: string;

    duration?: number;
    allowCancelTime?: number;

    imgKeyList?: string[];
    categoryIdList?: [];
    attributeIdList?: [];
    paymentMethodIdList?: string[];

    retailPrice?: number;
    sellingPrice?: number;
    currencyId?: string;

    relatedList?: [];

    priority?: number;
    status?: string;

    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    name?: string;
    description?: string;
    shop?: Shop;

    paymentMethodList?: Payment[];

    constructor(
        private languageService?: LanguageService,
        private shopService?: ShopService,
    ) {

    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.displayId = params.displayId || '';
        this.relativeIdList = params.relativeIdList || [];

        this.code = params.code || '';

        this.shopId = params.shopId || '';

        this.duration = params.duration || undefined;

        this.imgKeyList = params.imgKeyList || [];

        this.categoryIdList = params.categoryIdList || [];
        this.attributeIdList = params.attributeIdList || [];

        this.retailPrice = params.retailPrice || undefined;
        this.sellingPrice = params.sellingPrice || undefined;
        this.currencyId = params.discocurrencyIdunt || 'HKD';

        this.relatedList = params.relatedList || [];

        this.priority = params.priority || undefined;
        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];

        this.updatedAt = params.aupdatedAtge || undefined;
        this.createdAt = params.createdAt || undefined;

        this.name = await this.loadName(params);

        this.genre = params.genre || '';
    }

    async initFromDocument(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.displayId = params['Service ID'] || '';
        this.code = params['Service ID'] || '';

        this.shopId = params.shopId || '';

        this.categoryIdList = params.categoryIdList || [];
        this.attributeIdList = params.attributeIdList || [];

        this.duration = params['Duration'] || undefined;
        this.allowCancelTime = params['Time Required For Booking'] !== undefined ?
            parseInt(
                params['Time Required For Booking']
                    .replace(' hours', ''),
                10,
            )
            : undefined;

        const _retailPrice = params['Retail Price'] || undefined;
        if (_retailPrice) {
            this.retailPrice = parseFloat(
                _retailPrice.replace('HK$', ''),
            );
        }
        const _sellingPrice = params['Selling Price'] || undefined;
        if (_sellingPrice) {
            this.sellingPrice = Number(
                _sellingPrice.replace('HK$', ''),
            );
        }
        this.currencyId = params.currencyId || 'HKD';

        this.priority = params.priority || 10;

        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
        this.description = await this.loadDescription(params);

        this.genre = params['Type'] || '';
    }

    async setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    async loadName(params): Promise<string> {
        return this.languageService ? await this.languageService.getParam('service', params.id, 'name') : '';
    }

    async loadDescription(params): Promise<string> {
        return this.languageService ? await this.languageService.getParam('service', params.id, 'description') : '';
    }

    async setShopService(shopService: ShopService) {
        this.shopService = shopService;
    }

    async loadShop(params): Promise<Shop> {
        let shop = new Shop();
        if (params.shopId) {
            if (this.languageService) {
                shop = new Shop(this.languageService);
            }

            if (this.shopService) {

                const getShopParams = {
                    id: params.shopId,
                    status: 'enable',
                };
                const response = await this.shopService.get(getShopParams);
                const responseItem = response.pop();

                shop.init(responseItem);
            }
        }
        return shop;
    }

    async loadPaymentMethodList() {

    }

    async clean() {
        this.languageService = undefined;
        this.shopService = undefined;
    }
}
