
export class Project {
    id: String;
    code: String;
    description: String;
    settings: [];
    status: String;
    tagList: [];
    updatedAt: Number;
    createdAt: Number;

    constructor() {
    }

    async init(params) {
        this.id = params.id || '';
        this.code = params.code || '';
        this.description = params.description || '';
        this.settings = params.settings || [];
        this.status = params.status || '';
        this.tagList = params.tagList || [];
        this.updatedAt = params.updatedAt || undefined;
        this.createdAt = params.createdAt || undefined;
    }
}
