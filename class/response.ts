export class Response {
    statusCode: number;

    success?: string;
    error?: string;

    body?: ResponseBody;

    code: string;
    full_error: Array<any>;

    constructor(params) {
        this.statusCode = params.statusCode;

        this.success = params.success || undefined;
        this.body = new ResponseBody(params.body);

        this.error = params.error || undefined;
        this.code = params.code || undefined;
        this.full_error = params.full_error || undefined;
    }
}

export class ResponseBody {
    Item?: Array<any>;
    Items?: Array<any>;
    hits?: Array<any>;
    aggregations?: any;
    Count?: number;
    ScannedCount?: number;

    url?: string;
    key?: string;

    constructor(params) {
        if (params) {
            this.initialize(params);
        }
    }

    initialize(params) {
        this.Item = params.Item || undefined;
        this.Items = params.Items || undefined;
        this.hits = params.hits || undefined;

        this.Count = params.Count || params.total === undefined ? undefined : params.total.value || undefined;
        this.ScannedCount = params.ScannedCount || undefined;

        this.url = params.url || undefined;
        this.key = params.key || undefined;
    }
}
