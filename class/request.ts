import { environment } from '../../../environments/environment';
import { LanguageService } from '../services/language.service';

export class Request {
    id?: string;
    projectId?: string;

    userId?: string;

    title?: string;
    content?: string;
    keywords?: string[];

    imgKeyList?: string[];

    price?: number;
    currencyId?: string;

    priority?: number;

    status?: string;

    tagList: [];

    updatedAt?: number;
    createdAt?: number;

    name?: string;
    postedAt?: string;

    constructor(
        private languageService?: LanguageService,
    ) { }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.title = params.title || '';
        this.content = params.content || '';
        this.keywords = params.keywords || [];

        this.imgKeyList = params.imgKeyList || [];

        this.price = params.price || undefined;
        this.currencyId = params.currencyId || 'hkd';

        this.priority = params.priority || 10;

        this.status = params.status || 'opened';
        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
        this.postedAt = await this.loadDateTime(params);
    }

    getImgUrl(column) {
        return environment.media.url + '/' + this[column];
    }

    async setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    async loadName(params): Promise<string> {
        this.name = this.languageService ? await this.languageService.getParam('category', params.id, 'name') : '';
        return this.name;
    }

    async loadDateTime(params): Promise<string> {
        const createdAtDate = new Date(params.createdAt * 1000);
        if (this.isToday(createdAtDate)) {
            return createdAtDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
        }
        return createdAtDate.toLocaleDateString() || '';
    }

    isToday(checkingDate: Date) {
        const today = new Date();
        return checkingDate.getDate() == today.getDate() &&
            checkingDate.getMonth() == today.getMonth() &&
            checkingDate.getFullYear() == today.getFullYear()
    }
}
