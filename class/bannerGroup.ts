import { BannerStyle } from './bannerStyle';
import { SwiperOptions } from 'swiper';

export class BannerGroup {

    id?: string;
    projectId?: string;

    code?: string;

    criteriaType?: string;

    bannerStyle?: BannerStyle;
    swiperOptions?: SwiperOptions;

    version?: string;

    status?: string;
    tagList?: [];

    updatedAt?: number;
    createdAt?: number;

    constructor() {

    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.code = params.code || '';

        this.criteriaType = params.criteriaType || '';

        this.bannerStyle = params.bannerStyle || {};
        this.swiperOptions = params.swiperOptions || {};

        this.version = params.version || '';

        this.status = params.status || 'disable';
        this.tagList = params.tagList || [];
    }

}
