export class Language {
    id?: string;
    projectId?: string;
    locale?: string;
    application?: string;
    type?: string;
    code?: string;
    description?: string;
    imgKey?: string;
    document?: {};
    version?: string;
    status?: string;
    tagList?: [];

    constructor() {

    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.locale = params.locale || '';
        this.application = params.application || 'console';
        this.type = params.type || 'system';
        this.code = params.code || '';
        this.description = params.description || '';
        this.imgKey = params.imgKey || '';
        this.document = params.document || {};
        this.version = params.version || '';
        this.status = params.status || 'disable';
        this.tagList = params.tagList || [];
    }
}
