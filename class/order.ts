import { Product } from './product';
import { Service } from './service';
import { Shop } from './shop';

export class Order {
    id?: string;
    projectId?: string;

    userId?: string;

    merchantId?: string;
    shopId?: string;

    bookingIdList?: string[];

    product?: Product;
    service?: Service;
    shop?: Shop;

    qty?: number;
    amount?: number;

    paymentMethodId: string;
    paymentRefId: string;

    status?: 'opened' | 'initiated' | 'paid' | 'completed' | 'cancelled' | 'refunded' | 'onhold';
    tagList?: [];

    updatedAt?: number;
    createdAt?: number;


    constructor() {
    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.userId = params.userId || '';

        this.merchantId = params.merchantId || '';
        this.shopId = params.shopId || '';

        this.bookingIdList = params.bookingIdList || [];
        if (params.bookingId) {
            if (!this.bookingIdList) {
                this.bookingIdList = [params.bookingId];
            } else {
                this.bookingIdList.push(params.bookingId);
            }
        }

        this.product = new Product();
        if (params.product) {
            await this.product.init(params.product);
        }

        this.service = new Service();
        if (params.service) {
            await this.service.init(params.service);
        }

        this.shop = new Shop();
        if (params.shop) {
            await this.shop.init(params.shop);
        }

        this.paymentMethodId = params.paymentMethodId || '';

        this.paymentRefId = params.paymentRefId || '';

        this.amount = params.amount || undefined;

        this.status = params.status || 'opened';
        this.tagList = params.tagList || [];

        this.updatedAt = params.updatedAt;
        this.createdAt = params.createdAt;
    }
}
