export interface BannerStyle {
    item?: {
        borderRadius?: string;
        padding?: string;
        margin?: string;
    };
    padding?: string;
    margin?: string;
}