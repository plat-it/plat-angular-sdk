import { MerchantService } from './../services/merchant.service';
import { LanguageService } from '../services/language.service';

import { OpeningHour } from './openingHour';
import { Location } from './location';
import { Attribute } from './attribute';
import { Merchant } from './merchant';
export class Shop {
    id?: string;
    projectId?: string;

    displayId?: string;
    code?: string;

    merchantId?: string;
    businessRegistration?: string;
    businessRegistrationKey?: string;

    imgKeyList?: string[];
    email?: string;
    phone?: number;
    areaCode?: string;
    whatsapp?: number;

    street?: string;
    district?: string;
    state?: string;
    country?: string;
    location?: Location;
    sectorIdList?: string[];

    website?: string;
    openingHours?: OpeningHour[];
    attributeIdList?: string[];
    priority?: number;
    status?: string;
    tagList?: [];
    updatedAt?: number;
    createdAt?: number;

    name?: string;
    description?: string;
    attributeList?: Attribute[];

    constructor(
        params = {},
        private languageService?: LanguageService,
        private merchantService?: MerchantService,
    ) {
        this.init(params);
    }

    async init(params) {
        this.id = params.id || '';
        this.displayId = params.displayId || '';
        this.code = params.code || '';

        this.merchantId = params.merchantId || '';
        this.businessRegistration = params.businessRegistration || '';
        this.businessRegistrationKey = params.businessRegistrationKey || '';

        this.imgKeyList = params.imgKeyList || [];
        this.email = params.email || '';
        this.phone = params.phone || undefined;
        this.areaCode = params.areaCode || '';
        this.whatsapp = params.whatsapp || undefined;
        this.website = params.website || '';
        this.street = params.street || '';
        this.district = params.district || '';
        this.state = params.state || '';
        this.country = params.country || 'hk';
        this.location = new Location();
        this.location.init(params.location);
        this.sectorIdList = params.sectorIdList || [];

        this.openingHours = this.loadOpeningHours(params);
        this.attributeIdList = params.attributeIdList || [];
        this.priority = params.priority || undefined;
        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
    }

    async initFromDocument(params) {
        this.id = params.id || '';
        this.displayId = params['Shop ID'] || '';
        this.code = params['Shop ID'] || '';

        this.merchantId = params.merchantId || '';
        this.businessRegistration = params['BR No.'] || '';
        this.businessRegistrationKey = params.businessRegistrationKey || '';

        this.imgKeyList = params.imgKeyList || [];
        this.email = params['Email'] || '';
        this.phone = params['Shop Contact'] || undefined;
        this.areaCode = params.areaCode || '';
        this.whatsapp = params['WhatsApp'] || undefined;
        this.website = params['Website'] || '';
        this.street = params.street || '';
        this.district = params.district || '';
        this.state = params.state || '';
        this.country = params.country || 'hk';
        this.location = params.location || new Location();
        this.openingHours = this.loadOpeningHoursFromDocument(params);
        this.attributeIdList = params.attributeIdList || [];
        this.priority = params.priority || 10;
        this.status = params.status || 'disable';

        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
    }



    async setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    async loadName(params): Promise<string> {
        this.name = this.languageService ? await this.languageService.getParam('shop', params.id, 'name') : '';
        return this.name;
    }

    async loadDescription(params): Promise<string> {
        this.description = this.languageService ? await this.languageService.getParam('shop', params.id, 'description') : '';
        return this.description;
    }

    async setMerchantService(merchantService: MerchantService) {
        this.merchantService = merchantService;
    }

    async loadMerchant(params): Promise<Merchant> {
        let merchant = new Merchant();

        if (this.languageService) {
            merchant = new Merchant(this.languageService);
        }

        if (this.merchantService) {

            const getMerchantParams = {
                id: params.merchantId,
                status: 'enable',
            };
            const response = await this.merchantService.get(getMerchantParams);
            const responseItem = response.pop();

            merchant.init(responseItem);
        }
        return merchant;
    }


    async loadAddress(geoCode): Promise<boolean> {
        if (geoCode.location) {
            this.location = new Location(geoCode.location.lat, geoCode.location.lng);
        }

        for (const component of geoCode.address_components) {
            if (component.types.includes('neighborhood')) {
                this.district = component.short_name;
            }
            if (component.types.includes('administrative_area_level_1')) {
                this.state = component.short_name;
            }
            if (component.types.includes('country')) {
                this.country = component.short_name;
            }
        }
        return true;
    }

    loadOpeningHours(params): OpeningHour[] {
        const daysInWeek = 7;
        const openingHours: OpeningHour[] = [];

        if (params.openingHours) {
            for (const openingHour of params.openingHours) {
                const _openingHour = new OpeningHour(openingHour);
                openingHours.push(_openingHour);
            }
        } else {
            for (let i = 0; i < daysInWeek; i++) {
                openingHours.push(new OpeningHour());
            }
        }
        return openingHours;
    }

    loadOpeningHoursFromDocument(params): OpeningHour[] {
        const daysInWeek = 7;
        const openingHours: OpeningHour[] = [];

        if (params['Opening Hour']) {
            const openingHoursParams = params['Opening Hour'];
            for (let index = 0; index < daysInWeek; index++) {
                const _openingHour = new OpeningHour();

                if (openingHoursParams[index]) {
                    openingHoursParams[index].status = 'enable';
                    _openingHour.initFromDocument(openingHoursParams[index]);
                }

                openingHours[index] = _openingHour;
            }
        } else if (params['Opening Hour Week']) {
            const openingHoursParams = params['Opening Hour Week'];
            openingHoursParams.status = 'enable';

            for (let index = 0; index < daysInWeek; index++) {
                const _openingHour = new OpeningHour();

                _openingHour.initFromDocument(openingHoursParams);
                _openingHour.status = 'enable';

                openingHours[index] = _openingHour;
            }
        }

        return openingHours;
    }

    async clean() {
        this.languageService = undefined;
        this.merchantService = undefined;
    }
}
