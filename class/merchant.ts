import { LanguageService } from './../services/language.service';
export class Merchant {
    id?: string;
    projectId?: string;

    displayId?: string;
    code?: string;

    email?: string;
    emailVerified?: boolean;
    phone?: number;
    areaCode?: string;
    phoneVerified?: boolean;
    whatsapp?: number;
    whatsappVerified?: boolean;

    imgKey?: string;
    status?: string;
    settings?: {};
    tagList?: [];
    updatedAt?: number;
    createdAt?: number;

    name?: string;

    constructor(
        private languageService?: LanguageService,
    ) {

    }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.id || '';

        this.displayId = params.displayId || '';
        this.code = params.code || '';

        this.email = params.email || '';
        this.emailVerified = params.emailVerified || false;
        this.phone = params.phone || undefined;
        this.areaCode = params.areaCode || '';
        this.phoneVerified = params.phoneVerified || false;
        this.whatsapp = params.whatsapp || '';
        this.whatsappVerified = params.whatsappVerified || false;

        this.imgKey = params.imgKey || '';

        this.status = params.status || 'disable';

        this.settings = params.settings || {};

        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
    }

    async initFromDocument(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.displayId = params['Shop ID'] || '';
        this.code = params['Shop ID'] || '';

        this.email = params['Email'] || '';
        this.emailVerified = params.emailVerified || false;
        this.phone = params['Shop Contact'] || undefined;

        this.areaCode = params.areaCode || '';
        this.phoneVerified = params.phoneVerified || false;
        this.whatsapp = params['WhatsApp'] || undefined;

        this.whatsappVerified = params.whatsappVerified || false;

        this.imgKey = params.imgKey || '';

        this.status = params.status || 'disable';

        this.settings = params.settings || {};

        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
    }

    async setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    async loadName(params): Promise<string> {
        this.name = this.languageService ? await this.languageService.getParam('merchant', params.id, 'name') : '';
        return this.name;
    }


}
