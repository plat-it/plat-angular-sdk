import { environment } from '../../../environments/environment';
import { LanguageService } from './../services/language.service';

export class Sector {
    id?: string;
    projectId?: string;

    countryId?: string;

    code?: string;
    iconKey?: string;

    parentId?: string;

    status?: string;
    tagList: [];

    updatedAt?: number;
    createdAt?: number;

    name?: string;

    constructor(
        private languageService?: LanguageService,
    ) { }

    async init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';

        this.code = params.code || '';
        this.iconKey = params.iconKey || '';

        this.parentId = params.parentId || '0';
        this.status = params.status || 'disable';
        this.tagList = params.tagList || [];

        this.name = await this.loadName(params);
    }

    getImgUrl(column) {
        return environment.media.url + '/' + this[column];
    }

    async setLanguageService(languageService: LanguageService) {
        this.languageService = languageService;
    }

    async loadName(params): Promise<string> {
        this.name = this.languageService ? await this.languageService.getParam('sector', params.id, 'name') : '';
        return this.name;
    }

}
