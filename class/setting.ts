import { environment } from './../../../environments/environment';
import { LanguageService } from './../services/language.service';


export class Setting {
    id?: string;
    projectId?: string;
    application?: string;
    type?: string;
    code?: string;
    description?: string;
    imgKey?: string;
    document?: string;
    version?: string;
    status?: string;
    updatedAt?: Number;
    createdAt?: Number;

    constructor(private languageService?: LanguageService,
    ) { }

    init(params) {
        this.id = params.id || '';
        this.projectId = params.projectId || '';
        this.application = params.application || '';
        this.type = params.type || '';
        this.code = params.code || '';
        this.description = params.description || '';
        this.imgKey = params.imgKey || '';
        this.document = params.document || {};
        this.version = params.version || '';
        this.status = params.status || 'disable';
    }
}
