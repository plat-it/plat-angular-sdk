
export class Location {
    lat?: number;
    lng?: number;

    constructor(lat = 0, lng = 0) {
        this.lat = lat;
        this.lng = lng;
    }

    init(params = { lat: 0, lng: 0 }) {
        this.lat = params.lat;
        this.lng = params.lng;
    }

    async getGoogleLatLng(): Promise<google.maps.LatLng> {
        return new google.maps.LatLng(this.lat, this.lng);
    }
}
