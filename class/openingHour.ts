import { Time } from "./time";
export class OpeningHour {
    status?: string = 'disable';
    start?: Time = new Time();
    end?: Time = new Time();

    constructor(params?) {
        if (params) {
            this.init(params);
        }
    }

    async init(params) {
        if (params.start) {
            this.start.init(params.start);
        }
        if (params.end) {
            this.end.init(params.end);
        }
    }

    async initFromDocument(params) {
        if (params.start) {
            this.start.setTimeFromString(params.start);
        }
        if (params.end) {
            this.end.setTimeFromString(params.end);
        }

        this.status = params.status;
        this.start.status = params.status;
        this.end.status = params.status;
    }

    getDateFromTimeForDisplay() {
        return {
            start: this.start.getDisplayDate(),
            end: this.end.getDisplayDate(),
        };
    }

    getTimeFromDate(params) {
        if (params.start) {
            this.start.setTimeFromDate(new Date(params.start));
        }
        if (params.end) {
            this.end.setTimeFromDate(new Date(params.end));
        }
    }
}
