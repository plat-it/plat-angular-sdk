import { WebsocketService } from './services/helper/websocket.service';

import { CommonModule } from '@angular/common';

import {
    NgModule,
    ModuleWithProviders,
} from '@angular/core';

import { RestApiCrudHelperService } from './services/helper/rest-api-crud.service';

import { ProjectService } from './services/project.service';
import { SettingService } from './services/setting.service';

import { AttributeService } from './services/attribute.service';
import { CategoryService } from './services/category.service';

import { LanguageService } from './services/language.service';
import { LocaleService } from './services/locale.service';
import { MediaService } from './services/media.service';

import { StatusService } from './services/status.service';
import { GenderService } from './services/gender.service';

import { MerchantService } from './services/merchant.service';
import { ShopService } from './services/shop.service';
import { ServiceService } from './services/service.service';
import { ProductService } from './services/product.service';

import { GeoCodingService } from './services/geocoding.service';

import { UserService } from './services/user.service';
// import { OrdersService } from './services/orders.service';

import { CountryService } from './services/country.service';
import { SectorService } from './services/sector.service';

const SERVICES = [
    //   OrdersService,
    AttributeService,
    CategoryService,
    GeoCodingService,
    LanguageService,
    LocaleService,
    MediaService,
    MerchantService,
    ProjectService,
    RestApiCrudHelperService,
    ServiceService,
    ProductService,
    SettingService,
    ShopService,
    StatusService,
    GenderService,
    UserService,
    WebsocketService,

    SectorService,
    CountryService
];

@NgModule({
    imports: [
        CommonModule,
    ],
    providers: [
        ...SERVICES,
    ],
})
export class PLATProviderModule {
    static forRoot(): ModuleWithProviders<PLATProviderModule> {
        return {
            ngModule: PLATProviderModule,
            providers: [
                ...SERVICES,
            ],
        };
    }
}
