import { Injectable } from '@angular/core';
import { RestApiCrudHelperService } from './helper/rest-api-crud.service';


@Injectable()
export class MediaService {

    service = 'media';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
    ) { }

    async upload(files: File[]): Promise<string[]> {
        const urlList: string[] = [];
        for (const key of Object.keys(files)) {
            const initateUploadParams = {
                name: files[key].name,
                size: files[key].size,
                contentType: files[key].type,
                type: files[key].type,
            };
            const initateUploadresponseBody = await this.restApiHelper.updateData(
                this.service,
                initateUploadParams,
                '',
                {},
                {},
                true
            );
            if (
                !initateUploadresponseBody ||
                !initateUploadresponseBody.url
            ) {
                throw new Error('unable_to_initiate_upload');
            }

            await this.restApiHelper.putFile(initateUploadresponseBody.url.toString(), files[key]);

            urlList.push(initateUploadresponseBody.key);
        }

        return urlList;
    }
}
