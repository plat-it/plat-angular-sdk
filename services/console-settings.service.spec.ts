import { TestBed } from '@angular/core/testing';

import { ConsoleSettingsService } from './console-settings.service';

describe('ConsoleSettingsService', () => {
  let service: ConsoleSettingsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsoleSettingsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
