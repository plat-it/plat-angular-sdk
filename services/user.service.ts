import { Injectable } from '@angular/core';
import { AuthHelperService } from 'src/app/@core/helper/auth.service';

import { RestApiCrudHelperService } from '../../plat-angular-sdk/services/helper/rest-api-crud.service';

import { ProjectService } from './project.service';

import { User } from '../class/user';

@Injectable()
export class UserService {

    service = 'user';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private authHelperService: AuthHelperService
    ) {
    }

    async create(
        params = new User(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<User> {
        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: User = new User();

        if (responseBody.Item) {
            item.init(responseBody.Items);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<User[]> {
        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: User[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.Items) {
            for (const responseItem of responseBody.Items) {
                const user = new User();
                user.init(responseItem);
                items.push(user);
            }
        }

        if (responseBody.Item) {
            const user = new User();
            user.init(responseBody.Item);
            items.push(user);
        }
        return items;
    }


    async update(
        params = new User(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<User> {
        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: User = new User();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<User> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: User = new User();

        if (responseBody.Item) {
            console.log(responseBody.Item);
            item.init(responseBody.Item);
        }

        return item;
    }

    async getCurrentUser(): Promise<User> {
        const authId = await this.authHelperService.getCurrentTokenValue('id');
        const getUsersParams = {
            authId: authId
        };
        const responseValue = await this.get(getUsersParams);
        const item: User = new User();

        if (responseValue.length < 1) {
            throw new Error('invalid_user');
        }

        if (responseValue.length > 1) {
            throw new Error('user_not_unique');
        }

        item.init(responseValue[0]);

        return item;
    }
}

