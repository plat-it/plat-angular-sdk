import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class LocationService {

    localeList = [
        'zh-HK',
        'en-US',
    ];
    constructor() { }

    getLocaleList() {
        return this.localeList;
    }

}
