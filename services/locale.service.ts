import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class LocaleService {

    localeList = [
        'zh-HK',
        'en-US',
    ];
    constructor() { }

    getLocaleList() {
        return this.localeList;
    }

}
