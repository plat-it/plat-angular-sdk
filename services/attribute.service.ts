import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Attribute } from '../class/attribute';
@Injectable({
    providedIn: 'root',
})
export class AttributeService {

    service = 'attribute';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Attribute(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Attribute> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage, supressErrorMessage);
        const item: Attribute = new Attribute();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Attribute[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Attribute[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const attribute = new Attribute();
                attribute.init(responseBody.hits[key]._source);
                items.push(attribute);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const attribute = new Attribute();
                attribute.init(responseBody.Items[key]);
                items.push(attribute);
            }
        }

        if (responseBody.Item) {
            const attribute = new Attribute();
            attribute.init(responseBody.Item);
            items.push(attribute);
        }
        return items;
    }

    async update(
        params = new Attribute(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Attribute> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage);
        const item: Attribute = new Attribute();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Attribute> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        await this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Attribute = new Attribute();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async loadName(attributeList) {
        for (const attribute of attributeList) {
            await attribute.setLanguageService(this.languageService);
            await attribute.loadName(attribute);
        }

        return attributeList;
    }

    async getFromIdList(attributeIdList) {
        const attributeList = [];
        for (let attributeId of attributeIdList) {
            const getAttributeParam = {
                id: attributeId
            };
            attributeList.push((await this.get(getAttributeParam)).pop());
        }

        return attributeList;
    }
}
