import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Category } from '../class/category';
import { TreeNode } from '../class/tree';
@Injectable({
    providedIn: 'root',
})
export class CategoryService {

    service = 'category';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Category(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Category> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Category = new Category();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Category[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Category[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const category = new Category();
                category.init(responseBody.hits[key]._source);
                items.push(category);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const category = new Category();
                category.init(responseBody.Items[key]);
                items.push(category);
            }
        }

        if (responseBody.Item) {
            const category = new Category();
            category.init(responseBody.Item);
            items.push(category);
        }
        return items;
    }

    async mget(
        params = {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Category> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.mGetData(
            this.service,
            params,
            '',
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Category = new Category();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async update(
        params = new Category(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Category> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Category = new Category();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }


    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Category> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Category = new Category();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async getCategoryTree(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<TreeNode<Category>[]> {

        let completed = false;
        params['_from'] = 0;
        params['_limit'] = 100;

        let categoryList: Category[] = [];
        while (!completed) {
            const responseList = await this.get(
                params,
                supressSuccessMessage,
                supressErrorMessage,
            );

            if (responseList.length <= 0) {
                completed = true;
                break;
            }

            this.loadName(responseList);

            categoryList = categoryList.concat(responseList);

            params['_from'] = params['_from'] + params['_limit'];
        }

        const map = {};
        const data = [];

        const treeNodeList = [];

        for (let i = 0; i < categoryList.length; i++) {
            map[categoryList[i].id] = i;
            treeNodeList.push({
                'data': categoryList[i],
            });
        }

        for (const treeNode of treeNodeList) {
            const node = treeNode;
            if (node.data.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!treeNodeList[map[node.data.parentId]]) {
                    continue;
                }
                if (!treeNodeList[map[node.data.parentId]].children) {
                    treeNodeList[map[node.data.parentId]].children = [];
                }
                treeNodeList[map[node.data.parentId]].children.push(node);
            } else {
                data.push(node);
            }
        }
        return data;
    }

    list_to_tree(list) {
        const map = {};
        const roots = [];

        for (const listIndex of Object.keys(list)) {
            map[list.listIndex.id] = listIndex; // initialize the map
        }

        for (const node of list) {
            if (node.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!list[map[node.parentId]]) {
                    continue;
                }
                if (!list[map[node.parentId]].children) {
                    list[map[node.parentId]].children = [];
                }
                list[map[node.parentId]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    async loadName(categoryList: Category[]) {
        for (const category of categoryList) {
            await category.setLanguageService(this.languageService);
            await category.loadName(category);
        }

        return categoryList;
    }

    async validateCategoryList(categoryList: Category[] = []) {
        await this.loadName(categoryList);

        let index = categoryList.length;
        while (index--) {
            if (
                !(await this.categoryIsEnabled(categoryList[index])) ||
                !(await this.categoryhasImage(categoryList[index])) ||
                !(await this.categoryHasName(categoryList[index]))
            ) {
                categoryList.splice(index, 1);
            }
        }
        return categoryList;
    }

    async categoryIsEnabled(category: Category) {
        return category.status === 'enable';
    }

    async categoryhasImage(category: Category) {
        return category.iconKey;
    }

    async categoryHasName(category: Category) {
        return category.name && category.name !== "";
    }
}
