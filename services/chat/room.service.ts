import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from '../helper/rest-api-crud.service';

import { ProjectService } from '../project.service';
import { LanguageService } from '../language.service';

import { Room } from '../../class/room';
import { TreeNode } from '../../class/tree';
@Injectable({
    providedIn: 'root',
})
export class RoomService {

    service = '/chat/room';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Room(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Room> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Room = new Room();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Room[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Room[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const room = new Room();
                room.init(responseBody.hits[key]._source);
                items.push(room);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const room = new Room();
                room.init(responseBody.Items[key]);
                items.push(room);
            }
        }

        if (responseBody.Item) {
            const room = new Room();
            room.init(responseBody.Item);
            items.push(room);
        }
        return items;
    }

    async update(
        params = new Room(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Room> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Room = new Room();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Room> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Room = new Room();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }
}
