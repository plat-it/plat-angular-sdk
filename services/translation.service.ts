import { HttpClient, HttpHeaders, HttpParams, HttpParamsOptions } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Language } from '../class/language';
import { Response } from '../class/response';
import { NotificationService } from '../../@core/helper/notification.service';

@Injectable()
export class TranslationService implements TranslateLoader {


    constructor(
        private http: HttpClient,
        private notification: NotificationService,
    ) { }

    getTranslation(lang: string): Observable<any> {
        try {
            const url = environment.api.url + '/language';

            const getLanguageParms = {
                'locale': lang,
                'type': environment.api.setting.type,
                'application': environment.api.setting.application,
                'code': environment.api.setting.code,
                'status': 'enable',
            };

            const httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                }),
                params: getLanguageParms,
            };

            const response = this.http.get(url, httpOptions)
                .pipe(
                    map((response: Response) => {
                        const item = response.body.hits.pop()['_source'];
                        const language = new Language();
                        language.init(item);
                        return language.document;
                    }),
                    // catchError(_ => this.http.get(`/assets/i18n/zh-HK.json`)),
                );
            return response;
        } catch (error) {
            this.notification.handleError(error);
        }
    }
}
