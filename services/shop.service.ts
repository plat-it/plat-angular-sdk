import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Shop } from '../class/shop';
import { ResponseBody } from '../class/response';

@Injectable({
    providedIn: 'root',
})
export class ShopService {

    service = 'shop';
    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }


    async create(
        params = new Shop(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Shop> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Shop = new Shop();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }
        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Shop[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const items: Shop[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const shop = new Shop();
                shop.init(responseBody.hits[key]._source);
                items.push(shop);
            }
        }

        if (responseBody.Items) {
            for (const responseItem of responseBody.Items) {
                const shop = new Shop(this.languageService);
                shop.init(responseItem);
                items.push(shop);
            }
        }

        if (responseBody.Item) {
            const shop = new Shop(this.languageService);
            shop.init(responseBody.Item);
            items.push(shop);
        }
        return items;
    }

    async readRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        return await this.restApiHelper.updateData(
            this.service + '/readRaw',
            searchParams,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
    }
    async getItemsFromReadRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Shop[]> {
        const responseBody = await this.readRaw(
            searchParams,
            supressSuccessMessage,
            supressErrorMessage,
        );
        const items: Shop[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const shop = new Shop();
                shop.init(responseBody.hits[key]._source);
                items.push(shop);
            }
        }

        return items;
    }

    async update(
        params = new Shop(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Shop> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Shop = new Shop();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Shop> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Shop = new Shop();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async loadName(shopList) {
        for (const shop of shopList) {
            await shop.setLanguageService(this.languageService);
            await shop.loadName(shop);
        }

        return shopList;
    }

    async loadSectorList(shopList) {
        for (const shop of shopList) {
            await shop.setLanguageService(this.languageService);
            await shop.loadName(shop);
        }

        return shopList;
    }


    async validateShopList(shopList: Shop[] = []) {
        await this.loadName(shopList);

        let index = shopList.length;
        while (index--) {
            if (
                !(await this.shopIsEnabled(shopList[index])) ||
                !(await this.shophasImage(shopList[index])) ||
                !(await this.shopHasName(shopList[index]))
            ) {
                shopList.splice(index, 1);
            }
        }
        return shopList;
    }

    async shopIsEnabled(shop: Shop) {
        return shop.status === 'enable';
    }

    async shophasImage(shop: Shop) {
        return shop.imgKeyList.length > 0;
    }

    async shopHasName(shop: Shop) {
        return shop.name && shop.name !== "";
    }
}
