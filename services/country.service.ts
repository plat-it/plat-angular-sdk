import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Country } from '../class/country';
import { TreeNode } from '../class/tree';
@Injectable({
    providedIn: 'root',
})
export class CountryService {

    service = 'country';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Country(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Country> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Country = new Country();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Country[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Country[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const country = new Country();
                country.init(responseBody.hits[key]._source);
                items.push(country);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const country = new Country();
                country.init(responseBody.Items[key]);
                items.push(country);
            }
        }

        if (responseBody.Item) {
            const country = new Country();
            country.init(responseBody.Item);
            items.push(country);
        }
        return items;
    }

    async update(
        params = new Country(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Country> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Country = new Country();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Country> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Country = new Country();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async getCountryTree(
        params = {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<TreeNode<Country>[]> {

        let completed = false;
        params['_from'] = 0;
        params['_limit'] = 10;

        let countryList: Country[] = [];
        while (!completed) {
            const responseList = await this.get(
                params,
                supressSuccessMessage,
                supressErrorMessage,
            );

            if (responseList.length <= 0) {
                completed = true;
                break;
            }

            this.loadName(responseList);

            countryList = countryList.concat(responseList);

            params['_from'] = params['_from'] + params['_limit'];
        }

        const map = {};
        const data = [];

        const treeNodeList = [];

        for (let i = 0; i < countryList.length; i++) {
            map[countryList[i].id] = i;
            treeNodeList.push({
                'data': countryList[i],
            });
        }

        for (const treeNode of treeNodeList) {
            const node = treeNode;
            if (node.data.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!treeNodeList[map[node.data.parentId]]) {
                    continue;
                }
                if (!treeNodeList[map[node.data.parentId]].children) {
                    treeNodeList[map[node.data.parentId]].children = [];
                }
                treeNodeList[map[node.data.parentId]].children.push(node);
            } else {
                data.push(node);
            }
        }
        return data;
    }

    list_to_tree(list) {
        const map = {};
        const roots = [];

        for (const listIndex of Object.keys(list)) {
            map[list.listIndex.id] = listIndex; // initialize the map
        }

        for (const node of list) {
            if (node.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!list[map[node.parentId]]) {
                    continue;
                }
                if (!list[map[node.parentId]].children) {
                    list[map[node.parentId]].children = [];
                }
                list[map[node.parentId]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    async loadName(countryList: Country[]) {
        for (const country of countryList) {
            await country.setLanguageService(this.languageService);
            await country.loadName(country);
        }

        return countryList;
    }
}
