import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Response } from '../class/response';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';
import { NotificationService } from '../../@core/helper/notification.service';

import { Project } from '../class/project';

import { ActivatedRoute } from '@angular/router';
import { iif } from 'rxjs';

@Injectable()
export class ProjectService {

    service = 'project';

    defaultProjectCode = 'a_yau_b';

    constructor(
        private http: HttpClient,
        private notificatiion: NotificationService,
        private restApiHelper: RestApiCrudHelperService,
        private route: ActivatedRoute,
    ) {
    }

    async get(params?: {}, headers?: {}) {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            const url = environment.api.url + '/' + this.service;

            let httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                }),
            };

            if (headers) {
                httpOptions = await this.restApiHelper.addHeader(httpOptions, headers);
            }
            if (params) {
                httpOptions = await this.restApiHelper.addParams(httpOptions, params);
            }
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const getResponse = await this.http.get(url, httpOptions).toPromise();
            const response = new Response(getResponse);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            const responseBody = response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
            const items: Project[] = [];

            if (!responseBody) {
                return items;
            }

            if (responseBody.hits) {
                for (const key of Object.keys(responseBody.hits)) {
                    const project = new Project();
                    project.init(responseBody.hits[key]._source);
                    items.push(project);
                }
            }

            if (responseBody.Items) {
                for (const key of Object.keys(responseBody.Items)) {
                    const project = new Project();
                    project.init(responseBody.Items[key]);
                    items.push(project);
                }
            }

            if (responseBody.Item) {
                const project = new Project();
                project.init(responseBody.Item);
                items.push(project);
            }
            return items;

        } catch (error) {
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            this.notificatiion.handleError(error);
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

    getCodeFromUrl() {
        return this.route.snapshot.paramMap.get('code');
    }

    async getCurrentProject() {
        const project = await localStorage.getItem(this.service);
        if (
            project &&
            project !== '{}'
        ) {
            return JSON.parse(project);
        }
        return await this.setProjectLocalStorage();
    }

    async setProjectLocalStorage(projectCode?: '') {
        const project = new Project();

        const getParams = {
            code: projectCode || this.defaultProjectCode,
        };
        const getProjectResponse = await this.get(getParams);

        if (getProjectResponse.length <= 0) {
            throw new Error('logout_required');
        }
        const projectItem = getProjectResponse.pop();

        if (projectItem) {
            await project.init(projectItem);
            localStorage.setItem(this.service, JSON.stringify(project));
        }

        return project;
    }
}
