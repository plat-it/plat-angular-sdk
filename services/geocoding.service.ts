import { Injectable } from '@angular/core';

import { Location } from '../class/location';

import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

export class GeoCodingResponse {
    status?: string;
    addressList?: GeoCoding[] = [];

    constructor(params?: {}) {
        if (params) {
            this.init(params);
        }
    }
    async init(params) {
        this.status = params.status || null;
        for (const result of params.results) {
            console.log(result);
            const address = new GeoCoding(result);
            console.log(address);

            this.addressList.push(address);
        }
    }
}

export class GeoCoding {
    address_components?: AddressComponent[] = [];
    formatted_address?: string;
    location?: Location;

    constructor(params?: {}) {
        if (params) {
            this.init(params);
        }
    }

    async init(params) {
        console.log(params);
        this.address_components = params.address_components || [];
        this.formatted_address = params.formatted_address || '';
        if (params.geometry) {
            this.location = params.geometry.location || new Location();
        } else {
            this.location = new Location();
        }
    }
}

export class AddressComponent {
    long_name?: string;
    short_name?: string;
    types?: string[] = [];

    constructor(params?: {}) {
        if (params) {
            this.init(params);
        }
    }

    async init(params) {
        this.long_name = params.long_name || '';
        this.short_name = params.short_name || '';
        this.types = params.types || [];
    }

    typesContains(value: string) {
        return this.types.includes(value);
    }
}

@Injectable()
export class GeoCodingService {

    Location: Location;

    url: string = 'https://maps.googleapis.com/maps/api/geocode/json';

    constructor(
        private http: HttpClient,
    ) {
    }


    async initHttpOptions(params?: {}, headers?: {}) {
        let httpOptions = {
            headers: new HttpHeaders({
                'Accept-Language': 'zh-Hk, en',
            }),
        };

        if (headers) {
            httpOptions = await this.addHeader(httpOptions, headers);
        }
        if (params) {
            httpOptions = await this.addParams(httpOptions, params);
        }

        return httpOptions;
    }

    async addHeader(httpOptions, headers) {
        for (const key of Object.keys(headers)) {
            httpOptions.headers = httpOptions.headers.append(key, headers[key]);
        }
        return httpOptions;
    }

    async addParams(httpOptions, params) {
        httpOptions.params = new HttpParams();
        for (const key of Object.keys(params)) {
            httpOptions.params = httpOptions.params.append(key, params[key]);
        }
        return httpOptions;
    }

    async get(
        params?: {},
        headers?: {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<GeoCodingResponse> {
        // Http Options
        const httpOptions = await this.initHttpOptions(params, headers);
        const getResponse = await this.http.get(this.url, httpOptions).toPromise();

        return new GeoCodingResponse(getResponse);
    }

    async searchLocationFromAddress(address: string): Promise<GeoCoding[]> {
        const getLocationParams = {
            address: address,
            key: environment.google.api.key,
        };
        return (await this.get(getLocationParams)).addressList;

    }
}
