import { StorageService } from 'src/app/@core/helper/storage.service';
import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Setting } from '../class/setting';
@Injectable({
    providedIn: 'root',
})
export class SettingService {

    service = 'setting';
    storageKey = 'SETTINGS';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
        private storageService: StorageService,
    ) { }

    async create(
        params = new Setting(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Setting> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage, supressErrorMessage);
        const item: Setting = new Setting();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Setting> {

        let settingsStorage = await this.storageService.get(this.storageKey);
        if (settingsStorage) {
            const setting = new Setting();
            setting.init(settingsStorage);
            return setting;
        }

        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Setting[] = [];

        if (!responseBody) {
            throw new Error('settings.no_response_body');
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const setting = new Setting();
                setting.init(responseBody.hits[key]._source);
                items.push(setting);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const setting = new Setting();
                setting.init(responseBody.Items[key]);
                items.push(setting);
            }
        }

        if (responseBody.Item) {
            const setting = new Setting();
            setting.init(responseBody.Item);
            items.push(setting);
        }

        if (items.length < 1) {
            throw new Error('settings.not_found');
        }

        if (items.length > 1) {
            throw new Error('settings.more_than_one');
        }

        const setting = items.pop();
        this.storageService.set(
            this.storageKey,
            setting
        )

        return setting;
    }

    async update(
        params = new Setting(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Setting> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage);
        const item: Setting = new Setting();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Setting> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        await this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Setting = new Setting();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async loadName(settingList) {
        for (const setting of settingList) {
            await setting.setLanguageService(this.languageService);
            await setting.loadName(setting);
        }

        return settingList;
    }

    async getFromIdList(settingIdList) {
        return [];
    }
}
