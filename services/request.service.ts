import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Request } from '../class/request';
import { TreeNode } from '../class/tree';
@Injectable({
    providedIn: 'root',
})
export class RequestService {

    service = 'request';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Request(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Request> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Request = new Request();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Request[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Request[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const request = new Request();
                request.init(responseBody.hits[key]._source);
                items.push(request);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const request = new Request();
                request.init(responseBody.Items[key]);
                items.push(request);
            }
        }

        if (responseBody.Item) {
            const request = new Request();
            request.init(responseBody.Item);
            items.push(request);
        }
        return items;
    }

    async search(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Request[]> {
        const responseBody = await this.restApiHelper.updateData(
            this.service + '/readRaw',
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Request[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const request = new Request();
                request.init(responseBody.hits[key]._source);
                items.push(request);
            }
        }
        return items;
    }


    async update(
        params = new Request(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Request> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Request = new Request();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Request> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Request = new Request();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async getRequestTree(
        params = {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<TreeNode<Request>[]> {

        let completed = false;
        params['_from'] = 0;
        params['_limit'] = 10;

        let requestList: Request[] = [];
        while (!completed) {
            const responseList = await this.get(
                params,
                supressSuccessMessage,
                supressErrorMessage,
            );

            if (responseList.length <= 0) {
                completed = true;
                break;
            }

            this.loadName(responseList);

            requestList = requestList.concat(responseList);

            params['_from'] = params['_from'] + params['_limit'];
        }

        const map = {};
        const data = [];

        const treeNodeList = [];

        for (let i = 0; i < requestList.length; i++) {
            map[requestList[i].id] = i;
            treeNodeList.push({
                'data': requestList[i],
            });
        }

        for (const treeNode of treeNodeList) {
            const node = treeNode;
            if (node.data.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!treeNodeList[map[node.data.parentId]]) {
                    continue;
                }
                if (!treeNodeList[map[node.data.parentId]].children) {
                    treeNodeList[map[node.data.parentId]].children = [];
                }
                treeNodeList[map[node.data.parentId]].children.push(node);
            } else {
                data.push(node);
            }
        }
        return data;
    }

    list_to_tree(list) {
        const map = {};
        const roots = [];

        for (const listIndex of Object.keys(list)) {
            map[list.listIndex.id] = listIndex; // initialize the map
        }

        for (const node of list) {
            if (node.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!list[map[node.parentId]]) {
                    continue;
                }
                if (!list[map[node.parentId]].children) {
                    list[map[node.parentId]].children = [];
                }
                list[map[node.parentId]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    async loadName(requestList: Request[]) {
        for (const request of requestList) {
            await request.setLanguageService(this.languageService);
            await request.loadName(request);
        }

        return requestList;
    }

    async filterRequestList(requestList: Request[] = []) {
        await this.loadName(requestList);

        let index = requestList.length;
        while (index--) {
            if (
                // !(await this.requestIsEnabled(requestList[index])) ||
                !(await this.requestHasTitle(requestList[index])) ||
                !(await this.requestHasContent(requestList[index]))
            ) {
                requestList.splice(index, 1);
            }
        }
        return requestList;
    }

    async requestIsEnabled(request: Request) {
        return request.status === 'opened';
    }

    async requestHasTitle(request: Request) {
        return request.title && request.title !== "";
    }

    async requestHasContent(request: Request) {
        return request.content && request.content !== "";
    }
}
