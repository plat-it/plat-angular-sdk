import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Sector } from '../class/sector';
import { TreeNode } from '../class/tree';
@Injectable({
    providedIn: 'root',
})
export class SectorService {

    service = 'sector';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Sector(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Sector> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Sector = new Sector();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Sector[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Sector[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const sector = new Sector();
                sector.init(responseBody.hits[key]._source);
                items.push(sector);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const sector = new Sector();
                sector.init(responseBody.Items[key]);
                items.push(sector);
            }
        }

        if (responseBody.Item) {
            const sector = new Sector();
            sector.init(responseBody.Item);
            items.push(sector);
        }
        return items;
    }

    async update(
        params = new Sector(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Sector> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Sector = new Sector();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Sector> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Sector = new Sector();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async getSectorTree(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<TreeNode<Sector>[]> {

        let completed = false;

        let parentIdStack: string[] = ['0'];

        let numberofBranches = 1;

        let sectorList: Sector[] = [];
        while (parentIdStack.length) {
            while (parentIdStack.length) {
                params['parentId'] = parentIdStack.pop();

                const responseList = await this.get(
                    params,
                    supressSuccessMessage,
                    supressErrorMessage,
                );

                if (responseList.length <= 0) {
                    continue;
                }
                this.loadName(responseList);
                sectorList = [
                    ...sectorList,
                    ...responseList,
                ];
            }
            // Logic for iteration
            if (numberofBranches <= 0) {
                completed = true;
                break;
            }
            numberofBranches--;

            for (let sector of sectorList) {
                parentIdStack.push(sector.id);
            }
        }

        const map = {};
        const data = [];

        const treeNodeList = [];

        for (let i = 0; i < sectorList.length; i++) {
            map[sectorList[i].id] = i;
            treeNodeList.push({
                'data': sectorList[i],
            });
        }

        for (const treeNode of treeNodeList) {
            const node = treeNode;
            if (node.data.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!treeNodeList[map[node.data.parentId]]) {
                    continue;
                }
                if (!treeNodeList[map[node.data.parentId]].children) {
                    treeNodeList[map[node.data.parentId]].children = [];
                }
                treeNodeList[map[node.data.parentId]].children.push(node);
            } else {
                data.push(node);
            }
        }
        return data;
    }

    list_to_tree(list) {
        const map = {};
        const roots = [];

        for (const listIndex of Object.keys(list)) {
            map[list.listIndex.id] = listIndex; // initialize the map
        }

        for (const node of list) {
            if (node.parentId !== '0') {
                // if you have dangling branches check that map[node.parentId] exists
                if (!list[map[node.parentId]]) {
                    continue;
                }
                if (!list[map[node.parentId]].children) {
                    list[map[node.parentId]].children = [];
                }
                list[map[node.parentId]].children.push(node);
            } else {
                roots.push(node);
            }
        }
        return roots;
    }

    async loadName(sectorList: Sector[]) {
        for (const sector of sectorList) {
            await sector.setLanguageService(this.languageService);
            await sector.loadName(sector);
        }

        return sectorList;
    }
}
