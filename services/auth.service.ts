import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from '../../plat-angular-sdk/services/helper/rest-api-crud.service';
import { Auth } from '../class/auth';
import { ProjectService } from '../../plat-angular-sdk/services/project.service';


@Injectable({
    providedIn: 'root',
})
export class AuthService {

    service = 'auth';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
    ) { }

    async create(
        params, supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Auth> {
        params.projectId = (await this.projectService.getCurrentProject()).id;

        const responseBody = await this.restApiHelper.updateData(
            this.service + '/register',
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Auth = new Auth();

        if (
            responseBody &&
            responseBody.Item
        ) {
            item.init(responseBody.Item);
        }

        return item;
    }


    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Auth[]> {
        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Auth[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const auth = new Auth();
                auth.init(responseBody.Items[key]);
                items.push(auth);
            }
        }

        if (responseBody.Item) {
            const auth = new Auth();
            auth.init(responseBody.Item);
            items.push(auth);
        }
        return items;
    }

    async update(
        params,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Auth> {
        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Auth = new Auth();

        if (
            responseBody &&
            responseBody.Item
        ) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Auth> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Auth = new Auth();

        if (
            responseBody &&
            responseBody.Item
        ) {
            item.init(responseBody.Item);
        }

        return item;
    }
}
