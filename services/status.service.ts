import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { Status } from '../class/status';

@Injectable()
export class StatusService {

  statusList: Status[] = [];

  method = 'status';

  constructor(
    private restApiHelper: RestApiCrudHelperService,
) {
    this.initStatus();
    // this.restApiCrudService.initialise(this.method);
}

  getList(params = {}): Status[] {
    return this.statusList;
  }

  initStatus() {
    let status = new Status();
    status.init({name: 'enable'});
    this.statusList.push(status);
    status = new Status();
    status.init({name: 'disable'});
    this.statusList.push(status);
  }
}
