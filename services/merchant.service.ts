import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Merchant } from '../class/merchant';
@Injectable({
    providedIn: 'root',
})
export class MerchantService {

    service = 'merchant';
    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }


    async create(
        params = new Merchant(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Merchant> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Merchant = new Merchant();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }
        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Merchant[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Merchant[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const merchant = new Merchant();
                merchant.init(responseBody.hits[key]._source);
                items.push(merchant);
            }
        }

        if (responseBody.Items) {
            for (const responseItem of responseBody.Items) {
                const merchant = new Merchant();
                merchant.init(responseItem);
                items.push(merchant);
            }
        }

        if (responseBody.Item) {
            const merchant = new Merchant();
            merchant.init(responseBody.Item);
            items.push(merchant);
        }
        return items;
    }

    async update(
        params = new Merchant(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Merchant> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Merchant = new Merchant();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Merchant> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Merchant = new Merchant();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async loadName(merchantList) {
        for (const merchant of merchantList) {
            await merchant.setLanguageService(this.languageService);
            await merchant.loadName(merchant);
        }

        return merchantList;
    }
}
