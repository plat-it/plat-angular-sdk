import { ShopService } from './shop.service';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Service } from '../class/service';
import { ServiceOption } from '../class/serviceOption';
import { CategoryService } from './category.service';
import { ResponseBody } from '../class/response';
@Injectable({
    providedIn: 'root',
})
export class ServiceService {

    service = 'service';
    optionService = 'serviceOption';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
        private shopService: ShopService,
        private categoryService: CategoryService,
        private http: HttpClient,
    ) { }


    async create(
        params = new Service(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Service> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Service = new Service();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }
        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Service[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Service[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const service = new Service();
                service.init(responseBody.hits[key]._source);
                items.push(service);
            }
        }

        if (responseBody.Items) {
            for (const responseItem of responseBody.Items) {
                const service = new Service(this.languageService);
                service.init(responseItem);
                items.push(service);
            }
        }

        if (responseBody.Item) {
            const service = new Service();
            service.init(responseBody.Item);
            items.push(service);
        }
        return items;
    }

    async mget(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Service[]> {
        // const project = await this.projectService.getCurrentProject();
        // params.projectId = project.id;
        const url = environment.api.url + '/' + this.service + '/readMget';
        // Http Options
        const httpOptions = await this.restApiHelper.initHttpOptions(params);

        const postResponse = await this.http.post(url, JSON.stringify(params), httpOptions).toPromise();

        const responseBody = postResponse['body'];

        const items: Service[] = [];
        if (!responseBody) {
            return items;
        }
        console.log(responseBody);
        if (responseBody) {
            for (const key of Object.keys(responseBody)) {
                const service = new Service();

                if (responseBody[key].found) {
                    service.init(responseBody[key]._source);
                    items.push(service);
                }
            }
        }

        return items;
    }

    async readRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        return await this.restApiHelper.updateData(
            this.service + '/readRaw',
            searchParams,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
    }
    async getItemsFromReadRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Service[]> {
        const responseBody = await this.readRaw(
            searchParams,
            supressSuccessMessage,
            supressErrorMessage,
        );
        const items: Service[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const service = new Service();
                service.init(responseBody.hits[key]._source);
                items.push(service);
            }
        }

        return items;
    }


    async update(
        params = new Service(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Service> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Service = new Service();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Service> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Service = new Service();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async createOption(
        params = new ServiceOption(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<ServiceOption> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.optionService,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: ServiceOption = new ServiceOption();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }
        return item;
    }

    async getOption(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<ServiceOption[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.optionService,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: ServiceOption[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.Items) {
            for (const responseItem of responseBody.Items) {
                const serviceOption = new ServiceOption(this.languageService);
                serviceOption.init(responseItem);
                items.push(serviceOption);
            }
        }

        if (responseBody.Item) {
            const serviceOption = new ServiceOption();
            serviceOption.init(responseBody.Item);
            items.push(serviceOption);
        }
        return items;
    }

    async updateOption(
        params = new ServiceOption(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<ServiceOption> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.optionService,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: ServiceOption = new ServiceOption();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async deleteOption(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<ServiceOption> {
        const responseBody = await this.restApiHelper.deleteData(
            this.optionService,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: ServiceOption = new ServiceOption();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async loadName(serviceList: Service[]) {
        for (const service of serviceList) {
            await service.setLanguageService(this.languageService);
            service.name = await service.loadName(service);
        }

        return serviceList;
    }

    async loadShop(serviceList: Service[]) {
        for (const service of serviceList) {
            await service.setShopService(this.shopService);
            await service.loadShop(service);
        }

        return serviceList;
    }

    async getCategoryIdList(serviceList: Service[]) {
        let categoryIdList: string[] = [];
        for (const service of serviceList) {
            categoryIdList = [...new Set([...categoryIdList, ...service.categoryIdList])];
        }
        return categoryIdList;
    }

    async getCategoryStringList(categoryIdList: string[]) {
        let initiating = true;
        let categoryStringList = '';
        for (let categoryId of categoryIdList) {
            if (!initiating) {
                categoryStringList += ' / ';
            }
            categoryStringList += await this.getCategoryString(categoryId);

            initiating = false;
        }
        return categoryStringList
    }

    async getCategoryString(categoryId: string) {
        const getCategoryParams = {
            code: categoryId,
            type: 'category',

        };
        return await this.languageService.getParam('category', categoryId, 'name')
    }

    async validateServiceList(serviceList: Service[] = []) {
        await this.loadName(serviceList);

        let index = serviceList.length;
        while (index--) {
            if (
                !(await this.serviceIsEnabled(serviceList[index])) ||
                !(await this.servicehasImage(serviceList[index])) ||
                !(await this.serviceHasName(serviceList[index])) ||
                !(await this.serviceHasPrice(serviceList[index]))
            ) {
                serviceList.splice(index, 1);
            }
        }
        return serviceList;
    }

    async serviceIsEnabled(service: Service) {
        return service.status === 'enable';
    }

    async servicehasImage(service: Service) {
        return service.imgKeyList.length > 0;
    }

    async serviceHasName(service: Service) {
        return service.name && service.name !== "";
    }

    async serviceHasPrice(service: Service) {
        return service.retailPrice !== undefined;
    }

    async filterGenre(serviceList: Service[], genre: string) {
        let index = serviceList.length;
        while (index--) {
            if (
                serviceList[index].genre !== genre
            ) {
                serviceList.splice(index, 1);
            }
        }
        return serviceList;
    }

    async getShopIdListWithQuery(query): Promise<string[]> {
        console.log(query)
        const getServiceParams = {
            'query': query,
            "collapse": {
                "field": "shopId.keyword",
            },
            '_source': [
                'shopId'
            ]
        }
        const responseBody = await this.readRaw(getServiceParams);
        const shopIdList: string[] = [];
        if (!responseBody) {
            return shopIdList;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                shopIdList.push(responseBody.hits[key]._source.shopId);
            }
        }
        return shopIdList;
    }


}

