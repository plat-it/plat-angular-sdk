import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

import {
    HttpClient,
    HttpHeaders,
    HttpParams,
} from '@angular/common/http';

import { Response } from '../class/response';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';
import { NotificationService } from '../../@core/helper/notification.service';

import { Language } from '../class/language';

@Injectable({
    providedIn: 'root',
})
export class LanguageService {

    service = 'language';

    constructor(
        private http: HttpClient,
        private notificatiion: NotificationService,
        private restApiHelper: RestApiCrudHelperService,
        private translate: TranslateService,
    ) { }

    async get(
        params = {},
        headers = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Language[]> {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            const url = environment.api.url + '/' + this.service;

            let httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                }),
            };

            if (headers) {
                httpOptions = await this.restApiHelper.addHeader(httpOptions, headers);
            }
            if (params) {
                httpOptions = await this.restApiHelper.addParams(httpOptions, params);
            }
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const getResponse = await this.http.get(url, httpOptions).toPromise();
            const response = new Response(getResponse);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            if (!supressSuccessMessage) {
                this.notificatiion.handleSuccess(response.success);
            }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////

            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            const responseBody = response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
            const items: Language[] = [];
            if (!responseBody) {
                return items;
            }
            if (responseBody.hits) {
                for (const key of Object.keys(responseBody.hits)) {
                    const language = new Language();
                    language.init(responseBody.hits[key]._source);
                    items.push(language);
                }
            }

            if (responseBody.Items) {
                for (const key of Object.keys(responseBody.Items)) {
                    const language = new Language();
                    language.init(responseBody.Items[key]);
                    items.push(language);
                }
            }
            if (responseBody.Item) {
                const language = new Language();
                language.init(responseBody.Item);
                items.push(language);
            }
            return items;

        } catch (error) {
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }


    async getParam(type, code, key): Promise<string> {
        const getParams = {
            type: type,
            code: code,
            locale: this.translate.currentLang,
        };

        const getItems = await this.get(getParams);
        if (
            !getItems ||
            getItems.length !== 1
        ) {
            return '';
        }

        const language = getItems.pop();

        return language.document ? language.document[key] : '';
    }

    async create(
        params = new Language(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Language> {
        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Language = new Language();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }
        return item;
    }

    async update(
        params = new Language(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Language> {
        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Language = new Language();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        deleteId: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Language> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            deleteId,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Language = new Language();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async deleteWithParams(
        params: {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Language[]> {
        const itemList = [];
        const languageList = await this.get(params);
        for (const language of languageList) {
            const item = this.delete(
                language.id,
                supressSuccessMessage,
                supressErrorMessage,
            );
            itemList.push(item);
        }
        return itemList;
    }

    async search(
        searchString: string,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Language[]> {
        const params = {
            "query": {
                "term": {
                    "document.name.keyword": searchString,
                }
            }
        }
        const responseBody = await this.restApiHelper.updateData(
            this.service + '/readRaw',
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const items: Language[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const language = new Language();
                language.init(responseBody.hits[key]._source);
                items.push(language);
            }
        }

        return items;
    }

    async readRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Language[]> {
        const responseBody = await this.restApiHelper.updateData(
            this.service + '/readRaw',
            searchParams,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const items: Language[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const language = new Language();
                language.init(responseBody.hits[key]._source);
                items.push(language);
            }
        }

        return items;
    }
}
