import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { Gender } from '../class/gender';

@Injectable()
export class GenderService {

  genderList: Gender[] = [];

  method = 'gender';

  constructor(
    private restApiHelper: RestApiCrudHelperService,
) {
    this.initStatus();
    // this.restApiCrudService.initialise(this.method);
}

  getList(params = {}): Gender[] {
    return this.genderList;
  }

  initStatus() {
    let gender = new Gender();
    gender.init({name: 'female'});
    this.genderList.push(gender);
    gender = new Gender();
    gender.init({name: 'male'});
    this.genderList.push(gender);
  }
}
