import { Injectable } from '@angular/core';
import { MapPosition, MapPositionData } from '../../@core/data/map-position';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MarkerService extends MapPositionData {

  private position: MapPosition;
  private positionList: Observable<MapPosition[]>;

  public getMapPosition(): Observable<MapPosition[]> {
    return this.positionList;
  }

  public setMapPosition (lat, lng): MapPosition {
    this.position = {
      lat: lat,
      lng: lng,
    };
    return this.position;
  }
}
