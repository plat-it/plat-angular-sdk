import { TestBed } from '@angular/core/testing';

import { RestApiCrudHelperService } from './rest-api-crud.service';

describe('RestApiCrudHelperService', () => {
  let service: RestApiCrudHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestApiCrudHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
