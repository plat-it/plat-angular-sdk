import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class MediaHelperService {

    constructor() { }

    getImgUrlFromKey(key) {
        return key ? `${environment.media.url}/${ key }` : '';
    }

    getImgTagFromKey(key: string) {
        return key ? `<img width="80rem" src="${this.getImgUrlFromKey(key)}" />` : '';
    }

    getFirstImgTagFromKeyList(keyList: string[]) {
        return keyList ? this.getImgTagFromKey(keyList[0]) : '';
    }
}
