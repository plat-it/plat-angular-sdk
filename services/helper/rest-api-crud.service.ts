import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';

import {
    HttpClient,
    HttpHeaders,
    HttpParams,
} from '@angular/common/http';

import { Response, ResponseBody } from '../../class/response';

import { AuthHelperService } from '../../../@core/helper/auth.service';
import { NotificationService } from '../../../@core/helper/notification.service';

// import { fs } from 'fs';


@Injectable()
export class RestApiCrudHelperService {

    apiUrl: string;

    constructor(
        private http: HttpClient,
        private authHelperService: AuthHelperService,
        private notificatiion: NotificationService,
    ) {
        this.initialise();
    }

    async initialise() {
        await this.initialiseUrl();
    }

    async initialiseUrl() {
        this.apiUrl = environment.api.url;
    }

    async initHttpOptions(params?: {}, headers?: {}) {
        const defaultHeaders = {};
        defaultHeaders['Content-Type'] = 'application / json';

        const token = await this.authHelperService.getToken();
        if (token) {
            defaultHeaders['Authorization'] = token.toString();
        }

        let httpOptions = {
            headers: new HttpHeaders(defaultHeaders),
            params: new HttpParams(),
        };

        if (headers) {
            httpOptions = await this.addHeader(httpOptions, headers);
        }
        if (params) {
            httpOptions = await this.addParams(httpOptions, params);
        }

        return httpOptions;
    }

    async addHeader(httpOptions, headers) {
        for (const key of Object.keys(headers)) {
            if (headers[key] instanceof Array) {
                for (const paramValue of headers[key]) {
                    httpOptions.headers = httpOptions.headers.append(key, paramValue);
                }
            } else {
                httpOptions.headers = httpOptions.headers.append(key, headers[key]);
            }
            httpOptions.headers = httpOptions.headers.append(key, headers[key]);
        }
        return httpOptions;
    }

    async addParams(httpOptions, params) {
        httpOptions.params = new HttpParams();
        for (const key of Object.keys(params)) {
            if (params[key] instanceof Array) {
                for (const paramValue of params[key]) {
                    httpOptions.params = httpOptions.params.append(key, paramValue);
                }
            } else {
                httpOptions.params = httpOptions.params.append(key, params[key]);
            }
        }
        return httpOptions;
    }

    async createData(
        service: string,
        data = {},
        params?: {},
        headers?: {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            // URL
            const url = this.apiUrl + '/' + service;
            // Http Options
            const httpOptions = await this.initHttpOptions(params, headers);
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START /////////
            /////////////////////////////////////
            const putResponse = await this.http.put(url, JSON.stringify(data), httpOptions).toPromise();
            const response = new Response(putResponse);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            if (!supressSuccessMessage) {
                this.notificatiion.handleSuccess(response.success);
            }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////

            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            return response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
        } catch (error) {
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

    async readData(
        service: string,
        params?: {},
        headers?: {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            // URL
            const url = this.apiUrl + '/' + service;

            // Http Options
            const httpOptions = await this.initHttpOptions(params, headers);
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const getResponse = await this.http.get(url, httpOptions).toPromise();
            const response = new Response(getResponse);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            if (!supressSuccessMessage) {
                this.notificatiion.handleSuccess(response.success);
            }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            return response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
        } catch (error) {
            console.log(service);
            console.log(params);
            console.log(error);
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

    async mGetData(
        service: string,
        data?: {},
        headers?: {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            // URL
            const url = this.apiUrl + '/' + service + '/readMget';

            // Http Options
            const httpOptions = await this.initHttpOptions(data, headers);
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const getResponse = await this.http.post(url, httpOptions).toPromise();
            const response = new Response(getResponse);
            console.log(response);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            if (!supressSuccessMessage) {
                this.notificatiion.handleSuccess(response.success);
            }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            return response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
        } catch (error) {
            console.log(service);
            console.log(data);
            console.log(error);
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

    async updateData(
        service: string,
        data?: {},
        method = '',
        params?: {},
        headers?: {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            // URL
            let url = this.apiUrl + '/' + service;
            if (method === 'replace') {
                url += '/replace';
            }

            // Http Options
            const httpOptions = await this.initHttpOptions(params, headers);
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const postResponse = await this.http.post(url, JSON.stringify(data), httpOptions).toPromise();
            const response = new Response(postResponse);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            if (!supressSuccessMessage) {
                this.notificatiion.handleSuccess(response.success);
            }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////

            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            return response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
        } catch (error) {
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

    async deleteData(
        service: string,
        id: string,
        params?: {},
        headers?: {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            // URL
            const url = this.apiUrl + '/' + service;

            // Http Options
            const httpOptions = await this.initHttpOptions(params, headers);
            httpOptions['body'] = JSON.stringify({
                id: id,
            });
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const deleteResponse = await this.http.delete(url, httpOptions).toPromise();
            const response = new Response(deleteResponse);
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            if (!response.success) {
                throw new Error(response.error);
            }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            if (!supressSuccessMessage) {
                this.notificatiion.handleSuccess(response.success);
            }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////

            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            return response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
        } catch (error) {
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

    async putFile(
        url: string,
        file: File,
        params?: {},
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ) {
        try {
            /////////////////////////////////////
            // Initiate Variables -- START //////
            /////////////////////////////////////
            let httpOptions = {
                headers: { 'Content-Type': file.type },
                transformRequest: [],
            };

            if (params) {
                httpOptions = await this.addParams(httpOptions, params);
            }
            /////////////////////////////////////
            // Initiate Variables -- END ////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Perform Request -- START //////...
            /////////////////////////////////////
            const putResponse = await this.http.put(url, file, httpOptions).toPromise();
            /////////////////////////////////////
            // Perform Request -- END ///////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Validate Response -- START ///////
            /////////////////////////////////////
            // if (!response.success) {
            //     throw new Error(response.error);
            // }
            ////////////////////////////////////
            // Validate Response -- END /////////
            /////////////////////////////////////

            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////
            // if(!supressSuccessMessage){
            //     this.notificatiion.handleSuccess(response.success);
            // }
            /////////////////////////////////////
            // Display Success -- START //////.//
            /////////////////////////////////////

            /////////////////////////////////////
            // Return Response -- START //////.//
            /////////////////////////////////////
            // return response.body;
            /////////////////////////////////////
            // Return Response -- END //////.////
            /////////////////////////////////////
        } catch (error) {
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
            if (!supressErrorMessage) {
                this.notificatiion.handleError(error, false);
            }
            /////////////////////////////////////
            // Display Error -- START //////.////
            /////////////////////////////////////
        }
    }

}
