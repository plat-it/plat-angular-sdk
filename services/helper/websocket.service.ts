import { Injectable } from "@angular/core";
import { webSocket, WebSocketSubject } from "rxjs/webSocket";

import { environment } from "src/environments/environment";


export interface Message {
    source: string;
    content: string;
}

const WS_URL = environment.ws.url;

@Injectable()
export class WebsocketService {

    constructor() {

    }

    newInstance(): WebSocketSubject<any> {
        return webSocket(WS_URL);
    }

    connect(): WebSocketSubject<any> {
        const connection = this.newInstance();
        return connection;
    }

    // TODO
    disconnect(connection: WebSocketSubject<any>): void {
        connection.subscribe((connection) => {
            connection.onclose = function () { }; // disable onclose handler first
            connection.close();
        })
    }
}