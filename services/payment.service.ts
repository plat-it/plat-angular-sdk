import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Payment } from '../class/payment';

@Injectable({
    providedIn: 'root',
})
export class PaymentService {

    service = 'payment';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Payment(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Payment> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage, supressErrorMessage);
        const item: Payment = new Payment();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        body = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Payment[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Payment[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const payment = new Payment();
                payment.init(responseBody.hits[key]._source);
                items.push(payment);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const payment = new Payment();
                payment.init(responseBody.Items[key]);
                items.push(payment);
            }
        }

        if (responseBody.Item) {
            const payment = new Payment();
            payment.init(responseBody.Item);
            items.push(payment);
        }
        return items;
    }

    async update(
        params = new Payment(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Payment> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage);
        const item: Payment = new Payment();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Payment> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        await this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Payment = new Payment();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async loadName(paymentList) {
        for (const payment of paymentList) {
            await payment.setLanguageService(this.languageService);
            await payment.loadName(payment);
        }

        return paymentList;
    }
}
