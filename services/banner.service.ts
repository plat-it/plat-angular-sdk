import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Banner } from '../class/banner';
import { ResponseBody } from '../class/response';

@Injectable({
    providedIn: 'root',
})
export class BannerService {

    service = 'banner';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,
    ) { }

    async create(
        params = new Banner(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Banner> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Banner = new Banner();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Banner[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Banner[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const banner = new Banner();
                banner.init(responseBody.hits[key]._source);
                items.push(banner);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const banner = new Banner();
                banner.init(responseBody.Items[key]);
                items.push(banner);
            }
        }

        if (responseBody.Item) {
            const banner = new Banner();
            banner.init(responseBody.Item);
            items.push(banner);
        }
        return items;
    }

    async readRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<ResponseBody> {
        return await this.restApiHelper.updateData(
            this.service + '/readRaw',
            searchParams,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
    }
    async getItemsFromReadRaw(
        searchParams: any,
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Banner[]> {
        const responseBody = await this.readRaw(
            searchParams,
            supressSuccessMessage,
            supressErrorMessage,
        );
        const items: Banner[] = [];
        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const shop = new Banner();
                shop.init(responseBody.hits[key]._source);
                items.push(shop);
            }
        }

        return items;
    }

    async update(
        params = new Banner(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Banner> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Banner = new Banner();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Banner> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Banner = new Banner();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }
}
