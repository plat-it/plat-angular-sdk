import { ProductResolverService } from './../../@core/resolver/product-resolver.service';

import { Injectable } from '@angular/core';

import { RestApiCrudHelperService } from './helper/rest-api-crud.service';

import { ProjectService } from './project.service';
import { LanguageService } from './language.service';

import { Order } from '../class/order';

import { ServiceResolverService } from './../../@core/resolver/service-resolver.service';
import { BookingResolverService } from './../../@core/resolver/booking-resolver.service';

@Injectable({
    providedIn: 'root',
})
export class OrderService {

    service = 'order';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private languageService: LanguageService,

        private bookingResolver: BookingResolverService,
        private serviceResolver: ServiceResolverService,
        private productResolver: ProductResolverService,
    ) { }

    async create(
        params = new Order(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Order> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.createData(
            this.service,
            params,
            {},
            {},
            supressSuccessMessage, supressErrorMessage);
        const item: Order = new Order();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async get(
        params = {},
        body = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Order[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Order[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const order = new Order();
                order.init(responseBody.hits[key]._source);
                items.push(order);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const order = new Order();
                order.init(responseBody.Items[key]);
                items.push(order);
            }
        }

        if (responseBody.Item) {
            const order = new Order();
            order.init(responseBody.Item);
            items.push(order);
        }
        return items;
    }

    async scan(
        params = {},
        body = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Order[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service + '/scan',
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Order[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.hits) {
            for (const key of Object.keys(responseBody.hits)) {
                const order = new Order();
                order.init(responseBody.hits[key]._source);
                items.push(order);
            }
        }

        if (responseBody.Items) {
            for (const key of Object.keys(responseBody.Items)) {
                const order = new Order();
                order.init(responseBody.Items[key]);
                items.push(order);
            }
        }

        if (responseBody.Item) {
            const order = new Order();
            order.init(responseBody.Item);
            items.push(order);
        }
        return items;
    }

    async update(
        params = new Order(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Order> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage);
        const item: Order = new Order();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Order> {
        const deleteLanguageParams = {
            type: this.service,
            code: id,
        };
        await this.languageService.deleteWithParams(deleteLanguageParams);

        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Order = new Order();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async createOrder() {
        const createOrderParams = {
            product: this.productResolver.product,
            service: this.serviceResolver.service,
            booking: this.bookingResolver.booking,
            numberOfPeople: 1,
            status: 'opened'
        }
        const order = new Order();
        order.init(createOrderParams);

        this.create(order);
    }
}
