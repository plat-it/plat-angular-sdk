import { Injectable } from '@angular/core';
import { Permission } from '../class/permission';
import { RestApiCrudHelperService } from '../../plat-angular-sdk/services/helper/rest-api-crud.service';
import { ProjectService } from '../../plat-angular-sdk/services/project.service';
import { AuthHelperService } from 'src/app/@core/helper/auth.service';

@Injectable({
    providedIn: 'root',
})
export class PermissionService {

    service = 'permission';

    constructor(
        private restApiHelper: RestApiCrudHelperService,
        private projectService: ProjectService,
        private authHelperService: AuthHelperService,
    ) { }


    async create(
        params,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Permission> {
        params.projectId = (await this.projectService.getCurrentProject()).id;

        const responseBody = await this.restApiHelper.updateData(
            this.service + '/register',
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Permission = new Permission();

        if (responseBody.Item) {
            item.init(responseBody.Items);
        }

        return item;
    }

    async get(
        params = {},
        supressSuccessMessage: boolean = true,
        supressErrorMessage: boolean = false,
    ): Promise<Permission[]> {
        const project = await this.projectService.getCurrentProject();
        params['projectId'] = project.id;

        const responseBody = await this.restApiHelper.readData(
            this.service,
            params,
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const items: Permission[] = [];

        if (!responseBody) {
            return items;
        }

        if (responseBody.Items) {
            for (const responseItem of responseBody.Items) {
                const item = new Permission();
                item.init(responseItem);
                items.push(item);
            }
        }

        if (responseBody.Item) {
            const item = new Permission();
            item.init(responseBody.Item);
            items.push(item);
        }
        return items;
    }

    async update(
        params = new Permission(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Permission> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service,
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Permission = new Permission();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async updatePermissionRole(
        params = new Permission(),
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Permission> {
        const project = await this.projectService.getCurrentProject();
        params.projectId = project.id;

        const responseBody = await this.restApiHelper.updateData(
            this.service + '/permissionRole',
            params,
            '',
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );
        const item: Permission = new Permission();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }


    async delete(
        id: string,
        supressSuccessMessage: boolean = false,
        supressErrorMessage: boolean = false,
    ): Promise<Permission> {
        const responseBody = await this.restApiHelper.deleteData(
            this.service,
            id,
            {},
            {},
            supressSuccessMessage,
            supressErrorMessage,
        );

        const item: Permission = new Permission();

        if (responseBody.Item) {
            item.init(responseBody.Item);
        }

        return item;
    }

    async getCurrentPermission(): Promise<Permission> {
        const authId = await this.authHelperService.getCurrentTokenValue('id');

        const getPermissionParams = {
            authId: authId
        };
        const responseValue = await this.get(getPermissionParams);
        const item: Permission = new Permission();

        if (responseValue.length < 1) {
            throw new Error('invalid_permission');
        }

        if (responseValue.length > 1) {
            throw new Error('permission_not_unique');
        }

        item.init(responseValue[0]);

        return item;
    }
}
